# KAT3R8

eine virtuelle spielekonsole auf basis von python + pygame

geplant ist eine eigene programmiersprache dafür die an BASIC angelehnt ist aber erstmal muss python reichen


## usage

um das beispielprogramm auszuführen:

```bash
pip install click pygame numpy
python3 kat3r8.py example.py
```