def setup():
    global x
    global y
    global xdir
    global ydir
    global color
    global iterations
    x = 0
    y = 0
    xdir = 1
    ydir = 1
    color = 0
    iterations = 0
    clear(GRAY2)


def logic():
    global x
    global y
    global xdir
    global ydir
    global color
    global iterations
    string = 'kat3r8'
    if x + xdir + len(string) * 6 >= WIDTH:
        xdir = -1
        color = (color + 1) % 8
        iterations += 1
    elif x + xdir < 0:
        xdir = 1
        color = (color + 1) % 8
        iterations += 1
    if y + ydir + 6 >= HEIGHT:
        ydir = -1
        color = (color + 1) % 8
        iterations += 1
    elif y + ydir < 0:
        ydir = 1
        color = (color + 1) % 8
        iterations += 1
    x += xdir
    y += ydir

    clear(GRAY2 if color != GRAY2 else GRAY3)
    write(string, x, y, color)
    if iterations > 16:
        return False