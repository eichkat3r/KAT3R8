#!/usr/bin/env python3
import time

import click

import pygame
from pygame.locals import *

import numpy as np

#import cv2

from font import FONT


def make_color(color):
    assert color >= 0 and color < 8
    return color * 255 // 7


BLACK = 0
GRAY1 = 1
GRAY2 = 2
GRAY3 = 3
GRAY4 = 4
GRAY5 = 5
GRAY6 = 6
WHITE = 7


class KAT3R8:
    def __init__(self, screen_width, screen_height):
        self.buffer = np.zeros((screen_width, screen_height))
        self.buttons = np.zeros(8)
        #self.capture = cv2.VideoCapture(0)
        self.state = 'title'
        self.vars = {}

    def k8_pixel(self, x, y, color):
        self.buffer[x, y] = make_color(color)

    def k8_rect(self, x, y, w, h, color):
        screen_w, screen_h = self.buffer.shape
        rx0 = min(max(0, x), screen_w)
        rx1 = min(max(0, x + w), screen_w)
        ry0 = min(max(0, y), screen_h)
        ry1 = min(max(0, y + h), screen_h)
        self.buffer[rx0:rx1, ry0:ry1] = make_color(color)

    def k8_clear(self, color):
        self.buffer[:, :] = make_color(color)

    def k8_glyph(self, c, x, y, color):
        c = c.upper()
        assert c in FONT
        letter = FONT[c]
        for ly in range(5):
            for lx in range(5):
                if not letter[ly][lx]:
                    continue
                if x + lx < 0 or x + lx >= self.buffer.shape[0] or y + ly < 0 or y + ly >= self.buffer.shape[1]:
                    continue
                self.k8_pixel(x + lx, y + ly, color)

    def k8_write(self, text, x, y, color):
        tx = 1
        ty = 1
        text = text.upper()
        for c in text:
            if c == '\n':
                tx = 1
                ty += 6
                continue
            if c == ' ':
                tx += 6
            if c not in FONT:
                continue
            self.k8_glyph(c, x + tx, y + ty, color)
            tx += 6

    #def k8_webcam(self):
    #    ret, image = self.capture.read()
    #    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #    image = cv2.resize(image, (self.buffer.shape[0], self.buffer.shape[1]))
    #    self.buffer = image.T

    def k8_sleep(self, ms):
        time.sleep(ms * 0.001)

    #def __del__(self):
    #    self.capture.release()


def title_setup(kat3r8):
    kat3r8.vars['color'] = 0


def title_logic(kat3r8):
    kat3r8.vars['color'] = (kat3r8.vars['color'] + 1) % 4
    kat3r8.k8_clear(0)
    kat3r8.k8_write(' press space\n\nto insert coin', 16, 32, 2 + kat3r8.vars['color'])
    kat3r8.k8_sleep(100)


SCREEN_WIDTH = 128
SCREEN_HEIGHT = 128
SCREEN_SCALE = 4

def run_game(kat3r8, game_setup, game_logic):
    screen = pygame.display.set_mode((SCREEN_WIDTH * SCREEN_SCALE, SCREEN_HEIGHT * SCREEN_SCALE))
    pygame.display.set_caption('[[ KAT3R8 ]]')
    running = True
    capture = cv2.VideoCapture(0)
    title_setup(kat3r8)
    clock = pygame.time.Clock()

    while running:
        screen.fill((255, 255, 255))
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                elif event.key == K_SPACE:
                    if kat3r8.state == 'title':
                        game_setup()
                        kat3r8.state = 'game'
            elif event.type == QUIT:
                running = False

        if kat3r8.state == 'game':
            pressed =  pygame.key.get_pressed()
            kat3r8.buttons = np.zeros(8)
            if pressed[K_UP]:
                kat3r8.buttons[0] = 1
            elif pressed[K_DOWN]:
                kat3r8.buttons[1] = 1
            elif pressed[K_LEFT]:
                kat3r8.buttons[2] = 1
            elif pressed[K_RIGHT]:
                kat3r8.buttons[3] = 1
            elif pressed[K_SPACE]:
                kat3r8.buttons[4] = 1
            elif pressed[K_LCTRL]:
                kat3r8.buttons[5] = 1

        if kat3r8.state == 'title':
            title_logic(kat3r8)
        elif kat3r8.state == 'game':
            if game_logic() == False:
                kat3r8.state = 'title'

        scaled_buffer = kat3r8.buffer.repeat(SCREEN_SCALE, axis=0).repeat(SCREEN_SCALE, axis=1)
        rgb_buffer = np.stack((scaled_buffer, scaled_buffer, scaled_buffer), axis=-1)
        rgb_buffer = np.digitize(rgb_buffer, bins=np.arange(0, 256, 256 // 8)) * 255 / 8
        buffer_surface = pygame.surfarray.make_surface(rgb_buffer)
        screen.blit(buffer_surface, (0, 0))
        pygame.display.update()
        clock.tick(30)


@click.command()
@click.argument('gamefile')
def main(gamefile):
    with open(gamefile, 'r') as f:
        text = f.read()
    kat3r8 = KAT3R8(SCREEN_WIDTH, SCREEN_HEIGHT)
    _globals = {
        'set': set,
        'int': int,
        'float': float,
        'str': str,
        'complex': complex,
        'bool': bool,
        'list': list,
        'dict': dict,
        'bin': bin,
        'hex': hex,
        'oct': oct,
        'chr': chr,
        'ascii': ascii,
        'enumerate': enumerate,
        'len': len,
        'WHITE': WHITE,
        'BLACK': BLACK,
        'GRAY1': GRAY1,
        'GRAY2': GRAY2,
        'GRAY3': GRAY3,
        'GRAY4': GRAY4,
        'GRAY5': GRAY5,
        'GRAY6': GRAY6,
        'WIDTH': SCREEN_WIDTH,
        'HEIGHT': SCREEN_HEIGHT,
        '__builtins__': {
        }
    }
    prefix = 'k8'
    for method_name in dir(kat3r8):
        if not method_name.startswith(prefix):
            continue
        method = getattr(kat3r8, method_name)
        if not callable(method):
            continue
        _globals[method_name[len(prefix) + 1:]] = method
    _locals = {}
    code = compile(text, '<string>', 'exec')
    exec(code, _globals, _locals)
    game_setup = _locals['setup']
    game_logic = _locals['logic']
    run_game(kat3r8, game_setup, game_logic)


if __name__ == '__main__':
    main()
